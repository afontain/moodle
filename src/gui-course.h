/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#define HANDY_USE_UNSTABLE_API
#include <handy.h>

#include "data-struct.h"

G_BEGIN_DECLS

#define HDY_TYPE_GUI_COURSE (gui_course_get_type())

G_DECLARE_FINAL_TYPE (GuiCourse, gui_course, HDY, GUI_COURSE, HdyColumn)

GuiCourse* gui_course_new (struct Course course_info);

void update_course_content (GtkWidget *self_widget, gpointer unused);


G_END_DECLS
