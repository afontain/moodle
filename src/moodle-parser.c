/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <myhtml/myhtml.h>
#include <assert.h>
#include "moodle-parser.h"
#include "data-struct.h"
#include "config.h"
#include "utils.h"

static myhtml_t *myhtml;


static enum resource_type
read_link (const char *href, guint *id, gchar **filename)
{
  // yay C!
  if (sscanf (href, EPFL_ASSIGNMENT_LINK"%u", id))
    return RESOURCE_ASSIGNMENT;
  if (sscanf (href, EPFL_FILE_LINK"%u", id))
    return RESOURCE_FILE;
  if (sscanf (href, EPFL_FOLDERFILE_LINK"%m[^?]", id, filename))
    return RESOURCE_FOLDERFILE;
  if (sscanf (href, EPFL_FOLDER_LINK"%u", id))
    return RESOURCE_FOLDER;
  if (sscanf (href, EPFL_FORUM_LINK"%u", id))
    return RESOURCE_FORUM;
  if (sscanf (href, EPFL_PAGE_LINK"%u", id))
    return RESOURCE_PAGE;
  if (sscanf (href, EPFL_QUIZ_LINK"%u", id))
    return RESOURCE_QUIZ;
  if (sscanf (href, EPFL_URL_LINK"%u", id))
    return RESOURCE_URL;
  g_warning ("unknown kind of link, %s", href);
  return -1;
}

char *
moodle_parse_form_error (const gchar *html, gsize html_size)
{
  /*myhtml_collection_t *collection = myhtml_get_nodes_by_tag_id (tree, NULL, MyHTML_TAG_SCRIPT, NULL);

  if(collection && collection->list && collection->length) {
    myhtml_tree_node_t *text_node = myhtml_node_child (collection->list[0]);

    if(text_node) {
      const char* text = myhtml_node_text (text_node, NULL);

      if(text)
        printf ("Title: %s\n", text);
    }
  }

  g_print ("No tequila problem.");

    // release resources
  myhtml_collection_destroy (collection);*/

  return NULL;
}


struct FolderContent
moodle_parse_folder_content (const gchar *html, gsize html_size, const gchar *moodle_filename)
{
  myhtml_tree_t *tree;
  myhtml_tree_node_t *root_node; // the node we will look for urls into
  myhtml_collection_t *collection;
  myhtml_collection_t *hrefs;
  struct Resource *resources;

  if (!html_size || !html)
    g_error ("empty html given");

  tree = myhtml_tree_create ();
  myhtml_tree_init (tree, myhtml);
  myhtml_parse (tree, MyENCODING_UTF_8, html, html_size);

  collection = myhtml_get_nodes_by_attribute_value (tree, NULL, NULL, FALSE,
                                                    "role", strlen("role"),
                                                    "main", strlen("main"),
                                                    NULL);

  if(collection->length==1)
    root_node = collection->list[0];
  else
    g_error ("weird result getting the folder container\n");
  myhtml_collection_destroy (collection);

  hrefs = myhtml_get_nodes_by_tag_id_in_scope (tree, NULL,
                                               root_node,
                                               MyHTML_TAG_A,
                                               NULL);

  if (hrefs->length == 0)
    g_error ("weird result getting folder's links\n");

  resources = malloc (hrefs->length * sizeof (struct Resource)); // at most this size will be used.
  size_t count=0;
  enum resource_type type;
  gchar *name=NULL, *href=NULL, *filename=NULL;
  guint id;

  for (size_t i=0; i<hrefs->length; i++) {
    myhtml_tree_node_t *node = hrefs->list[i];

    for (myhtml_tree_attr_t *attr = myhtml_node_attribute_first (node); attr; attr = attr->next) {
      if (strcmp (myhtml_attribute_key (attr, NULL), "href") == 0) {
        href = g_strdup (myhtml_attribute_value (attr, NULL)); // found!
        break;
      }
    }

    if (href) {
      type = read_link (href, &id, &filename);
      if (node->child && node->child->next && node->child->next->child) {
        name = g_strdup (myhtml_node_text(node->child->next->child, NULL));
      }
      if (!name) {
        g_warning ("Could not find the name of a file from a folder\n");
        name = filename;
      }
      resources[count] = (struct Resource) {type, id, name, href, filename};
      count++;
    }
  }
  resources = realloc (resources, count * sizeof (struct Resource));

  myhtml_collection_destroy (hrefs);
  myhtml_tree_destroy (tree);

  struct FolderContent content = {count, resources};
  return content;
}


struct Courses
moodle_parse_course_list (const gchar *html, gsize html_size, const gchar *filename)
{
  myhtml_tree_t *tree;
  myhtml_tree_node_t *courses_node; // the node we will look for urls into
  myhtml_collection_t *collection;
  myhtml_collection_t *hrefs;
  struct Course *courses;

  if (!html_size || !html)
    g_error ("empty html given");

  tree = myhtml_tree_create ();
  myhtml_tree_init (tree, myhtml);
  myhtml_parse (tree, MyENCODING_UTF_8, html, html_size);

  collection = myhtml_get_nodes_by_attribute_value (tree, NULL, NULL, FALSE,
                                                    "id", strlen("id"),
                                                    "coc-courselist", strlen("coc-courselist"),
                                                    NULL);

  if (collection->length==1)
    courses_node = collection->list[0];
  else
    g_error ("weird result getting the courses container\n");
  myhtml_collection_destroy (collection);

  hrefs = myhtml_get_nodes_by_tag_id_in_scope (tree, NULL,
                                               courses_node,
                                               MyHTML_TAG_A,
                                               NULL);

  if (hrefs->length == 0)
    g_error ("weird result getting courses links\n");

  courses = malloc (hrefs->length * sizeof (struct Course)); // at most this size will be used.
  size_t count=0;
  size_t size;
  const char *string;
  for (size_t i=0; i<hrefs->length; i++) {

    string = NULL;
    for (myhtml_tree_attr_t *attr = myhtml_node_attribute_first (hrefs->list[i]); attr; attr = attr->next) {
      if (strcmp (myhtml_attribute_key (attr, NULL), "href") == 0) {
        // we found the href attribute
        if (g_str_has_prefix (myhtml_attribute_value (attr, NULL), EPFL_COURSE_LINK))
          string = myhtml_attribute_value (attr, &size); // yay!
        break;
      }
    }

    if (string && size) {
      // nice link, we add it!
      sscanf(string, EPFL_COURSE_LINK"%u", &(courses[count].id));

      string = myhtml_node_text (myhtml_node_child (hrefs->list[i]), &size);

      if (G_UNLIKELY (!string || !size)) {
        g_print ("failed to get course name\n");
        continue;
      }
      courses[count].name = g_strndup (string, size);
      count++;
    }
  }
  courses = realloc (courses, sizeof (struct Course) * count);

  myhtml_collection_destroy (hrefs);
  myhtml_tree_destroy (tree);

  return (struct Courses) {count, courses};
}


static struct Week
moodle_extract_week (gchar *week_name, myhtml_tree_t *tree, myhtml_tree_node_t *div)
{
  myhtml_tree_node_t *li;
  guint resource_count = 0;
  struct Resource *resources = NULL;
  for (myhtml_tree_node_t *element = div->child; element; element = element->next) {
    if (element->tag_id == MyHTML_TAG_UL) {
      // files
      resources = malloc (sizeof (struct Resource) * 1000);

      for (li = element->child; li; li = li->next) {
        gchar *name, *textname, *filename;
        enum resource_type type;
        guint id;
        myhtml_collection_t *hrefs = myhtml_get_nodes_by_tag_id_in_scope (tree, NULL,
                                                                          li,
                                                                          MyHTML_TAG_A,
                                                                          NULL);
        if (!hrefs || !hrefs->length) {
          // no <a> in this week
          myhtml_collection_destroy (hrefs);
          continue;
        }

        myhtml_tree_node_t *node = hrefs->list[0];
        name = textname = filename = NULL;
        id = 0;
        for (myhtml_tree_attr_t *attr = myhtml_node_attribute_first (node); attr; attr = attr->next) {
          if (strcmp (myhtml_attribute_key (attr, NULL), "href") == 0) {
            const char *href = myhtml_attribute_value (attr, NULL);
            type = read_link (href, &id, &filename);
            name = g_strdup (/*g_path_get_basename (*/href/*)*/);
            if (node->child && node->child->next && node->child->next->child) {
              textname = g_strdup (myhtml_node_text(node->child->next->child, NULL));
            }
            break;
          }
        }

        if (id && textname && name) {
          assert(resource_count<1000); // there should be no more than 1000 files a week, right ? right ??
          resources[resource_count] = (struct Resource) {type, id, textname, name, filename};
          resource_count++;
        } else {
          g_print ("a li doesn't have any link\n");
        }

        myhtml_collection_destroy (hrefs);
      }
      resources = realloc (resources, sizeof (struct Resource)*resource_count);
    }
  }

  return (struct Week) {week_name, resource_count, resources};
}


struct CourseContent
moodle_parse_course_content (const gchar *html, gsize html_size, const gchar *filename)
{
  myhtml_tree_t *tree;
  myhtml_collection_t *root_col;
  myhtml_tree_node_t *root_node; // the node we will look for urls into
  myhtml_collection_t *weeks_col;
  guint week_count;
  struct Week *weeks;

  if (!html_size || !html)
    g_error ("empty html given\n");

  tree = myhtml_tree_create ();
  myhtml_tree_init (tree, myhtml);
  myhtml_parse (tree, MyENCODING_UTF_8, html, html_size);

  root_col = myhtml_get_nodes_by_attribute_value (tree, NULL, NULL, FALSE,
                                                  "role", strlen("role"),
                                                  "main", strlen("main"),
                                                  NULL);
  if(root_col->length==1)
    root_node = root_col->list[0];
  else
    g_error ("weird result getting the courses' root element\n");
  myhtml_collection_destroy (root_col);


  weeks_col = myhtml_get_nodes_by_attribute_value_begin (tree, NULL, root_node, FALSE,
                                                         "id", strlen("id"),
                                                         "section-", strlen("section-"),
                                                         NULL);

  if (weeks_col->length == 0) {
    myhtml_collection_destroy (weeks_col);
    g_warning ("%s seems to have no course section\n", filename);
    struct CourseContent empty = {0, NULL};
    return empty;
  }

  week_count = weeks_col->length;

  weeks = malloc (sizeof (struct Week) * (week_count+1));

  for (size_t i=0; i<week_count; i++) {
    // each element contain several div. We need to find the right one
    char* week_name = NULL;
    for (myhtml_tree_attr_t* attr = myhtml_node_attribute_first (weeks_col->list[i]); attr; attr = attr->next) {
      if (strcmp(myhtml_attribute_key (attr, NULL), "aria-label") == 0) {
        week_name = g_strdup (myhtml_attribute_value (attr, NULL));
        break;
      }
    }

    for (myhtml_tree_node_t *div = weeks_col->list[i]->child; div; div = div->next) {
      for (myhtml_tree_attr_t* attr = myhtml_node_attribute_first (div); attr; attr = attr->next) {
        if (strcmp(myhtml_attribute_key (attr, NULL), "class") == 0
         && strcmp(myhtml_attribute_value (attr, NULL), "content") == 0) {
          // we reached the actual week content !
          weeks[i] = moodle_extract_week (week_name, tree, div);
        }
      } // foreach attr
    } // foreach div
  } // foreach week
  myhtml_collection_destroy (weeks_col);
  myhtml_tree_destroy (tree);

  struct CourseContent content = {week_count, weeks};
  return content;
}


void
moodle_parser_init (void)
{
  myhtml = myhtml_create ();
  myhtml_init (myhtml, MyHTML_OPTIONS_DEFAULT, 2, 0);
}

