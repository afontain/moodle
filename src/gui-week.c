/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "gui-week.h"
#include "gui-resource.h"

#define HANDY_USE_UNSTABLE_API
#include <handy.h>

struct _GuiWeek
{
  GtkBox parent_instance;
  //HdyActionRow *text;
  HdyActionRow *placeholder;
  GtkLabel *week_label;
  const gchar *week_name;
  GtkListBox *week_content;
  guint resource_count;
  struct Resource* resources;
};

enum {
  PROP_0, PROP_WEEK_NAME, PROP_WEEK_RESOURCE_COUNT, PROP_WEEK_RESOURCES, N_WEEK_PROPERTIES
};

GParamSpec *gui_week_properties[N_WEEK_PROPERTIES] = { NULL, };


G_DEFINE_TYPE (GuiWeek, gui_week, GTK_TYPE_BOX)


GuiWeek* gui_week_new (struct Week week_info)
{
  return g_object_new (HDY_TYPE_GUI_WEEK,
                       "week_name", week_info.week_name,
                       "resource_count", week_info.resource_count,
                       "resources", week_info.resources,
                       NULL);
}


static void
update_files (GuiWeek* self)
{
  for (guint i=0; i<self->resource_count; i++) {
    GuiResource* file = gui_resource_new (self->resources[i]);
    gtk_container_add (GTK_CONTAINER (self->week_content), GTK_WIDGET (file));
  }
}

static void
gui_week_set_property (GObject      *object,
                       guint         property_id,
                       const GValue *value,
                       GParamSpec   *pspec)
{
  GuiWeek *self = HDY_GUI_WEEK (object);

  switch (property_id) {
  case PROP_WEEK_NAME:
    self->week_name = g_value_get_string (value);
    gtk_label_set_label (self->week_label, self->week_name);
    break;
  case PROP_WEEK_RESOURCE_COUNT:
    self->resource_count = g_value_get_uint (value);
    break;
  case PROP_WEEK_RESOURCES:
    self->resources = g_value_get_pointer (value);
    update_files (self);
    break;
  default:
    // We don't have any other property...
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void
gui_week_class_init (GuiWeekClass *class)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  gui_week_properties[PROP_WEEK_NAME] =
    g_param_spec_string ("week_name", "week_name",
                         "The name of the 'week' we are in, like «20–27april» or «Exercices for the semester»",
                         NULL,
                         G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);

  gui_week_properties[PROP_WEEK_RESOURCE_COUNT] =
    g_param_spec_uint ("resource_count", "resource_count",
                       "The number of resources the *resources points to",
                       0, G_MAXUINT,
                       0,
                       G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);

  gui_week_properties[PROP_WEEK_RESOURCES] =
    g_param_spec_pointer ("resources", "resources",
                          "The usable resources: files, folders",
                          G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);

  object_class->set_property = gui_week_set_property;
  g_object_class_install_properties (object_class,
                                     N_WEEK_PROPERTIES,
                                     gui_week_properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/ch/gnugen/Moody/ui/gui-week.ui");
  gtk_widget_class_bind_template_child (widget_class, GuiWeek, week_label);
  gtk_widget_class_bind_template_child (widget_class, GuiWeek, week_content);
  gtk_widget_class_bind_template_child (widget_class, GuiWeek, placeholder);
  gtk_widget_class_bind_template_callback_full (widget_class, "file_selected_cb", G_CALLBACK(activated_cb));
}

static void
gui_week_init (GuiWeek *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
  gtk_list_box_set_header_func (self->week_content, hdy_list_box_separator_header, NULL, NULL);

  // don't take too much space
  GtkWidget *box = gtk_container_get_children (GTK_CONTAINER (self->placeholder))->data;
  g_object_set (G_OBJECT (box),
                "height-request", -1,
                "margin", 0,
                "margin-left",  12,
                "margin-right", 12,
                NULL);
}
