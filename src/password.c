/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <libsecret/secret.h>
#include "password.h"
#include "config.h"

const static SecretSchema * moodle_provider_get_schema (void) G_GNUC_CONST;
#define MOODLE_SCHEMA  moodle_provider_get_schema ()


gboolean
moodle_password_set (const gchar *username, const gchar *password)
{
  return secret_password_store_sync (MOODLE_SCHEMA,
                                     NULL,
                                     "Moodle",
                                     password,
                                     NULL,
                                     NULL,
                                     "username", username,
                                     NULL);
}


gboolean
moodle_password_get (const gchar *username, gchar **password)
{
  GError *error = NULL;
  *password = secret_password_lookup_sync (MOODLE_SCHEMA,
                                           NULL,
                                           NULL,
                                           "username", username,
                                           NULL);
  if (error != NULL) {
    g_warning ("%s\n", error->message);
    return FALSE; // we couldn't get password
  }
  return TRUE;
}


void
moodle_password_free (gchar *password)
{
  //secret_password_wipe (password);
}


// https://developer.gnome.org/libsecret/0.18/libsecret-SecretSchema.html
const static SecretSchema *
moodle_provider_get_schema (void)
{
  static const SecretSchema schema = {
    APP_NAME, SECRET_SCHEMA_NONE,
    {
      {  "username", SECRET_SCHEMA_ATTRIBUTE_STRING },
      {  NULL, 0 },
    }
  };
  return &schema;
}

