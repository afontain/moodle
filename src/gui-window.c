/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "gui-window.h"
#include "gui-pass-dialog.h"
#include "gui-course.h"
#include "data-struct.h"
#include "settings.h"
#include "moodle-provider.h"
#include "config.h"

#include <glib/gi18n.h>
#define HANDY_USE_UNSTABLE_API
#include <handy.h>

struct _GuiWindow
{
  GtkApplicationWindow parent_instance;

  MoodSettings *settings;
  struct Course* courses;
  guint course_count;

  HdyLeaflet *header_box;
  HdyLeaflet *content_box;
  GtkButton *back;
  GtkMenuButton *menu;
  //GtkToggleButton *search_button;
  GtkStackSidebar *sidebar;
  GtkStack *stack;
  //HdySearchBar *search_bar;
  //GtkEntry *search_entry;
  HdyHeaderGroup *header_group;
  GuiPassDialog *dialog;
};

G_DEFINE_TYPE (GuiWindow, gui_window, GTK_TYPE_APPLICATION_WINDOW)


static void update_moodle_index (GuiWindow *self);

static void
initialize_moodle_provider (MoodSettings *settings)
{
  g_autofree const gchar *username, *password;
  g_object_get (settings, "username", &username,
                          "password", &password, NULL);
  moodle_provider_init (MOODLE_MODE_ONLINE, username, password);
}

static void
pass_changed_cb (gpointer ptr)
{
  GuiWindow *self = ptr;

  moodle_provider_disconnect ();
  initialize_moodle_provider (self->settings);
  update_moodle_index (self);
}

static void
activate_ask_pass (GSimpleAction *simple,
                   GVariant      *parameter,
                   gpointer       self_ptr)
{
  GuiWindow *self = self_ptr;
  self->dialog = gui_pass_dialog_new (GTK_WINDOW (self), self->settings);
  gtk_widget_show (GTK_WIDGET (self->dialog));
}

/*
static void
activate_print_string (GSimpleAction *simple,
                       GVariant      *parameter,
                       gpointer       user_data)
{
  g_print ("%s\n", g_variant_get_string (parameter, NULL));
}
*/

static gboolean
gui_window_key_pressed_cb (GtkWidget   *sender,
                           GdkEventKey *event,
                           GuiWindow   *self)
{
  GdkModifierType default_modifiers = gtk_accelerator_get_default_mod_mask ();
  guint keyval;
  GdkModifierType state;

  gdk_event_get_keyval ((GdkEvent *) event, &keyval);
  gdk_event_get_state ((GdkEvent *) event, &state);

  switch (state & default_modifiers) {

  case GDK_CONTROL_MASK:
    switch (keyval) {

    // quit
    case GDK_KEY_q:
      gtk_widget_destroy (GTK_WIDGET (self));
      break;

    // go to first course
    case GDK_KEY_Begin:
    case GDK_KEY_KP_Begin:
    case GDK_KEY_Home:
    case GDK_KEY_KP_Home:
      if (self->course_count > 0) {
        const gchar *name = self->courses[0].name;
        gtk_stack_set_visible_child_name (self->stack, name);
      }
      break;

    // go to last course
    case GDK_KEY_End:
    case GDK_KEY_KP_End:
      if (self->course_count > 0) {
        const gchar *name = self->courses[self->course_count-1].name;
        gtk_stack_set_visible_child_name (self->stack, name);
      }
      break;

    // go to previous course
    case GDK_KEY_Page_Up:
    case GDK_KEY_KP_Page_Up:
      if (self->course_count > 1) { // that's a little hackish, but works well enough
        const gchar *name = gtk_stack_get_visible_child_name (self->stack);
        for (guint i=0; i<self->course_count; i++) {
          if (g_strcmp0 (name, self->courses[i].name) != 0) {
            if (i > 0) { // we don't go before the first element
              const gchar *target_name = self->courses[i-1].name;
              gtk_stack_set_visible_child_name (self->stack, target_name);
            }
            break;
          }
        }
      }
      break;

    // go to next course
    case GDK_KEY_Page_Down:
    case GDK_KEY_KP_Page_Down:
      if (self->course_count > 1) { // that's a little hackish, but works well enough
        const gchar *name = gtk_stack_get_visible_child_name (self->stack);
        for (guint i=0; i<self->course_count; i++) {
          if (!g_strcmp0 (name, self->courses[i].name)) {
            if (i < self->course_count-1) { // we don't go past the last
              const gchar *target_name = self->courses[i+1].name;
              gtk_stack_set_visible_child_name (self->stack, target_name);
            }
            break;
          }
        }
      }
      break;

    default:
      return FALSE;
    }
    break;

  default:
    return FALSE;
  }

  return TRUE;
}

static void
destroy_widget (GtkWidget *w, gpointer unused) {
  gtk_widget_destroy (w);
}
static void
ellipsize_label (GtkWidget *list_item, gpointer unused) {
  GtkLabel *label = GTK_LABEL (gtk_bin_get_child (GTK_BIN (list_item)));
  gtk_label_set_ellipsize (label, PANGO_ELLIPSIZE_END);
}

static void
update_courses (GuiWindow *self)
{
  // clean up the current stack
  gtk_container_foreach (GTK_CONTAINER (self->stack), destroy_widget, NULL);

  // and replace its content
  for (int i=0; i<self->course_count; i++) {
    GuiCourse *course = gui_course_new (self->courses[i]);
    gtk_stack_add_titled (self->stack,
                          GTK_WIDGET (course),
                          self->courses[i].name,
                          self->courses[i].name);
  }

  // ellipsize the labels, this is a workaround.
  // See https://source.puri.sm/Librem5/libhandy/issues/118 for the bug report
  // and https://gitlab.gnugen.ch/afontain/moodle/issues/20
  GtkWidget *scrolled_win = gtk_bin_get_child (GTK_BIN (self->sidebar));
  GtkWidget *viewport     = gtk_bin_get_child (GTK_BIN (scrolled_win));
  GtkWidget *label_list   = gtk_bin_get_child (GTK_BIN (viewport));

  gtk_container_foreach (GTK_CONTAINER (label_list), ellipsize_label, NULL);
}

static void
get_courses_cb (gpointer ptr, struct Courses courses)
{
  GuiWindow* self = ptr;
  self->courses = courses.courses;
  self->course_count = courses.course_count;
  update_courses (self);
}

static void
gui_window_notify_visible_child_cb (GObject    *sender,
                                    GParamSpec *pspec,
                                    GuiWindow  *self)
{
  hdy_leaflet_set_visible_child_name (self->content_box, "content");
  gtk_container_foreach (GTK_CONTAINER (self->stack), update_course_content, NULL);
}

static void
gui_window_back_clicked_cb (GtkWidget *sender,
                            GuiWindow *self)
{
  hdy_leaflet_set_visible_child_name (self->content_box, "sidebar");
}


GuiWindow *
gui_window_new (GtkApplication *application)
{
  return g_object_new (HDY_TYPE_GUI_WINDOW, "application", application, NULL);
}


static void
gui_window_finalize (GObject *object)
{
  // this way we avoid segfaulting on exit when some requests aren't fulfilled yet
  moodle_provider_disconnect ();
  G_OBJECT_CLASS (gui_window_parent_class)->finalize (object);
}


static void
gui_window_class_init (GuiWindowClass *class)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  object_class->finalize = &gui_window_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/ch/gnugen/Moody/ui/gui-window.ui");
  gtk_widget_class_bind_template_child (widget_class, GuiWindow, header_box);
  gtk_widget_class_bind_template_child (widget_class, GuiWindow, content_box);
  gtk_widget_class_bind_template_child (widget_class, GuiWindow, back);
  gtk_widget_class_bind_template_child (widget_class, GuiWindow, menu);
  //gtk_widget_class_bind_template_child (widget_class, GuiWindow, search_button);
  gtk_widget_class_bind_template_child (widget_class, GuiWindow, sidebar);
  gtk_widget_class_bind_template_child (widget_class, GuiWindow, stack);
  //gtk_widget_class_bind_template_child (widget_class, GuiWindow, search_bar);
  //gtk_widget_class_bind_template_child (widget_class, GuiWindow, search_entry);
  gtk_widget_class_bind_template_child (widget_class, GuiWindow, header_group);
  gtk_widget_class_bind_template_callback_full (widget_class, "key_pressed_cb", G_CALLBACK(gui_window_key_pressed_cb));
  gtk_widget_class_bind_template_callback_full (widget_class, "notify_visible_child_cb", G_CALLBACK(gui_window_notify_visible_child_cb));
  gtk_widget_class_bind_template_callback_full (widget_class, "back_clicked_cb", G_CALLBACK(gui_window_back_clicked_cb));
}


static void
update_moodle_index (GuiWindow *self)
{
  switch (moodle_provider_get_courses_sync (&get_courses_cb, self)) {
    case MOODLE_ERROR_OK:
      break;
    case MOODLE_ERROR_CONNECTION:
      g_warning ("Could not reach Moodle\n");
      break;
    case MOODLE_ERROR_AUTH:
      g_warning ("Could not authenticate; is password incorrect?\n");
      break;
    default:
      g_assert (0);
  }
}

static void
gui_window_init (GuiWindow *self)
{
  g_set_prgname(APP_NAME);
  self->course_count = 0;
  self->dialog = NULL;
  gtk_widget_init_template (GTK_WIDGET (self));

  self->settings = mood_settings_new ();

  g_signal_connect_swapped (self->settings, "notify::password",
                            G_CALLBACK (pass_changed_cb), self);

  const GActionEntry entries[] = {
    { "ask-pass",     activate_ask_pass          },
    //{ "print-string", activate_print_string, "s" }
  };
  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   entries, G_N_ELEMENTS (entries),
                                   self);

  hdy_leaflet_set_can_swipe_back (self->header_box, TRUE);
  hdy_leaflet_set_can_swipe_back (self->content_box, TRUE);

  initialize_moodle_provider (self->settings);

  update_moodle_index (self);

  hdy_leaflet_set_visible_child_name (self->content_box, "sidebar");
}
