/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "gui-pass-dialog.h"

struct _GuiPassDialog
{
  HdyDialog parent_instance;

  MoodSettings *settings;

  GtkButton *accept;
  GtkButton *cancel;
  GtkEntry *user_entry;
  GtkEntry *pass_entry;
  GtkLabel *user_label;
  GtkLabel *pass_label;
};

G_DEFINE_TYPE (GuiPassDialog, gui_pass_dialog, HDY_TYPE_DIALOG)

GuiPassDialog *
gui_pass_dialog_new (GtkWindow    *window,
                     MoodSettings *settings)
{
  GuiPassDialog *self = g_object_new (HDY_TYPE_GUI_PASS_DIALOG,
                                      "use-header-bar", 1,
                                      NULL);

  self->settings = settings;

  g_autofree const gchar *current_username;
  g_object_get (self->settings, "username", &current_username, NULL);
  // prefill the username
  gtk_entry_set_text (self->user_entry, current_username);

  gtk_window_set_transient_for (GTK_WINDOW (self), window);
  return self;
}

static gboolean
disable_if_empty (GBinding *binding,
                  const GValue *from_value,
                  GValue *to_value,
                  gpointer user_data)
{
  guint length = g_value_get_uint (from_value);
  g_value_set_boolean (to_value, length != 0);
  return TRUE; // success
}

static void
dialog_close_cb (GuiPassDialog *self, gint response)
{
  switch (response) {
  case GTK_RESPONSE_DELETE_EVENT:
  case GTK_RESPONSE_CANCEL:
    break;
  case GTK_RESPONSE_ACCEPT:
    g_object_set (self->settings,
                  "username", gtk_entry_get_text (self->user_entry),
                  "password", gtk_entry_get_text (self->pass_entry), NULL);
    break;
  default:
    g_assert (0);
  }
  gtk_widget_destroy (GTK_WIDGET (self));
}

static void
gui_pass_dialog_class_init (GuiPassDialogClass *class)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);

  gtk_widget_class_set_template_from_resource (widget_class, "/ch/gnugen/Moody/ui/gui-pass-dialog.ui");
  gtk_widget_class_bind_template_child (widget_class, GuiPassDialog, accept);
  gtk_widget_class_bind_template_child (widget_class, GuiPassDialog, cancel);
  gtk_widget_class_bind_template_child (widget_class, GuiPassDialog, user_entry);
  gtk_widget_class_bind_template_child (widget_class, GuiPassDialog, pass_entry);
  gtk_widget_class_bind_template_child (widget_class, GuiPassDialog, user_label);
  gtk_widget_class_bind_template_child (widget_class, GuiPassDialog, pass_label);
  gtk_widget_class_bind_template_child (widget_class, GuiPassDialog, user_entry);
  gtk_widget_class_bind_template_child (widget_class, GuiPassDialog, pass_entry);
}

static void
gui_pass_dialog_init (GuiPassDialog *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
  g_signal_connect (G_OBJECT (self), "response", G_CALLBACK (dialog_close_cb), NULL);

  // we render the accept button insensitive when the password is empty
  g_object_bind_property_full (self->pass_entry, "text-length",
                               self->accept, "sensitive",
                               G_BINDING_SYNC_CREATE,
                               disable_if_empty,
                               NULL, NULL, NULL);
}
