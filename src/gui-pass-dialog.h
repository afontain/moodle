/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "settings.h"

#include <gtk/gtk.h>
#define HANDY_USE_UNSTABLE_API
#include <handy.h>

#define HDY_TYPE_GUI_PASS_DIALOG (gui_pass_dialog_get_type())

G_DECLARE_FINAL_TYPE (GuiPassDialog, gui_pass_dialog, HDY, GUI_PASS_DIALOG, HdyDialog)

GuiPassDialog *gui_pass_dialog_new (GtkWindow *win, MoodSettings *settings);
