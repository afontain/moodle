/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gio/gio.h>     // GInputStream stuff

#include "utils.h"
#include "data-struct.h"

#define CHUNK_SIZE 100000

void
read_file (const char *name, char **content, size_t *size)
{
  g_autoptr (GFile) file = g_file_new_for_uri (name);
  GInputStream *is;
  is = G_INPUT_STREAM (g_file_read (file, NULL, NULL));
  if (!G_IS_INPUT_STREAM (is))
    g_error ("Could not read file %s from cache\n", name);
  read_g_input_stream (is, content, size);
}


static void
write_file_cb (GObject      *object,
               GAsyncResult *res,
               gpointer      content)
{
  GFile *file = (GFile*) object;
  g_autoptr (GError) error = NULL;

  if (!g_file_replace_contents_finish (file, res, NULL, &error))
    g_print ("Impossible to write to %s, %s\n", g_file_get_uri (file), error->message);
  g_free (content);
}

void
write_file_async (const char *name, char *content, size_t size)
{
  g_autoptr (GFile) file = g_file_new_for_uri (name);
  g_file_replace_contents_async (file,
                                 content,
                                 size,
                                 NULL,
                                 FALSE,
                                 G_FILE_CREATE_PRIVATE,
                                 NULL,
                                 write_file_cb,
                                 content);
}


void
read_g_input_stream (GInputStream *is, char **content, size_t *size)
{
  char buffer[CHUNK_SIZE];
  size_t buffer_size;

  size_t used_size = 0;
  size_t allocated_size = CHUNK_SIZE;
  *content = malloc (CHUNK_SIZE);
  *size = 0;

  while ((buffer_size = g_input_stream_read (is, buffer, CHUNK_SIZE, NULL, NULL))) {
    if (buffer_size==-1)
      g_error ("Error reading input stream");

    if (used_size+buffer_size >= allocated_size) {
      allocated_size *= 2;
      *content = realloc (*content, allocated_size);
    }
    memcpy (used_size+*content, buffer, buffer_size);
    used_size += buffer_size;
  }

  g_object_unref (is);
  *content = realloc (*content, used_size+1);
  *size = used_size;
}


void
write_g_input_stream_to_file (GInputStream *is, const char *filename)
{
  g_autoptr (GFile) file = g_file_new_for_uri (filename);
  g_autoptr (GError) error = NULL;
  g_autoptr (GFileOutputStream) os =
    g_file_replace (file, NULL, FALSE, G_FILE_CREATE_PRIVATE, NULL, &error);

  if (error) {
    g_warning ("error opening file '%s' for writing: %s", filename, error->message);
  } else {
    g_output_stream_splice (G_OUTPUT_STREAM (os), is,
                            G_OUTPUT_STREAM_SPLICE_NONE, //G_OUTPUT_STREAM_SPLICE_CLOSE_SOURCE|G_OUTPUT_STREAM_SPLICE_CLOSE_TARGET,
                            NULL,
                            &error);
    if (error)
      g_warning ("error writing file '%s': %s", filename, error->message);
  }

  g_object_unref (is);
}
