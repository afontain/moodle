/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "gui-course.h"
#include "gui-week.h"
#include "moodle-provider.h"

#include "data-struct.h"


struct _GuiCourse
{
  HdyActionRow parent_instance;
  gint maximum_width;
  gint linear_growth_width;
  GtkBox *box_week;
  GtkLabel *title;
  //GtkLabel *description; // subtitle? reason? abstract? motivate?
  guint course_id;
  gboolean content_obtained;
  struct CourseContent content;
};

enum {
  PROP_0, PROP_COURSE_NAME, PROP_COURSE_ID, N_COURSE_PROPERTIES
};

GParamSpec *gui_course_properties[N_COURSE_PROPERTIES] = { NULL, };


G_DEFINE_TYPE (GuiCourse, gui_course, HDY_TYPE_COLUMN)


GuiCourse *
gui_course_new (struct Course course_info)
{
  return g_object_new (HDY_TYPE_GUI_COURSE,
                       "course_name", course_info.name,
                       "course_id",   course_info.id,
                       NULL);
}

static void destroy_widget (GtkWidget *w, gpointer nothing) {
  gtk_widget_destroy (w);
}

void
update_course_content (GtkWidget *self_widget, gpointer unused)
{
  if (!HDY_IS_GUI_COURSE (self_widget))
    return;
  GuiCourse *self = HDY_GUI_COURSE (self_widget);
  GtkStack *stack = GTK_STACK (gtk_widget_get_parent (self_widget));

  // clean up the current content
  gtk_container_foreach (GTK_CONTAINER (self->box_week), &destroy_widget, NULL);

  if (self_widget != gtk_stack_get_visible_child (stack))
    return; // we aren't shown, let's not show anything

  for (guint i=0; i<self->content.week_count; i++) {
    GuiWeek *week = gui_week_new (self->content.weeks[i]);
    gtk_box_pack_start (self->box_week,
                        GTK_WIDGET (week),
                        FALSE,
                        FALSE,
                        0);
  }
}

static void
course_content_obtained_cb (gpointer ptr, gpointer data)
{
  GuiCourse *self = ptr;
  struct CourseContent *content = data;
  self->content_obtained = TRUE;
  self->content = *content;
  update_course_content (GTK_WIDGET (self), NULL);
}

static void
gui_course_set_property (GObject      *object,
                         guint         property_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  GuiCourse *self = HDY_GUI_COURSE (object);

  switch (property_id) {
  case PROP_COURSE_NAME:
    gtk_label_set_label (self->title, g_value_get_string (value));
    break;
  case PROP_COURSE_ID:
    self->course_id = g_value_get_uint (value);
    moodle_provider_get_course_content_async (course_content_obtained_cb, self->course_id, object);
    break;
  default:
    // We don't have any other property...
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void
gui_course_class_init (GuiCourseClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);

  gui_course_properties[PROP_COURSE_NAME] =
    g_param_spec_string ("course_name", "name",
                         "The name of the course (like Math 101)",
                         NULL,
                         G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);

  gui_course_properties[PROP_COURSE_ID] =
    g_param_spec_uint ("course_id", "id",
                       "The id of the course (like 12345)",
                       0, G_MAXUINT,
                       0,
                       G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);

  object_class->set_property = gui_course_set_property;
  g_object_class_install_properties (object_class,
                                     N_COURSE_PROPERTIES,
                                     gui_course_properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/ch/gnugen/Moody/ui/gui-course.ui");
  gtk_widget_class_bind_template_child (widget_class, GuiCourse, title);
  gtk_widget_class_bind_template_child (widget_class, GuiCourse, box_week);
  //gtk_widget_class_bind_template_child (widget_class, GuiCourse, description);
}

static void
gui_course_init (GuiCourse *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->content_obtained = FALSE;
}
