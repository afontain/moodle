/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <libsoup/soup.h>
#include "moodle-provider.h"
#include "moodle-parser.h"
#include "utils.h"
#include "config.h"

#include <glib/gstdio.h> // g_mkdir

typedef struct MoodleProviderState {
  SoupSession *session;
  SoupCookieJar *jar;
  SoupLogger *logger;
  const gchar *username;
  const gchar *password;
  MoodleOnlineMode online_mode;
  gboolean connected;
} MoodleProviderState;
static MoodleProviderState self;


typedef void (*handle_stream_cb) (GInputStream       *is,
                                  gboolean            should_save_to_disk,
                                  const gchar        *filename,
                                  SoupMessageHeaders *headers,
                                  provider_cb         cb,
                                  gpointer            user_data);

struct cb_data {
  handle_stream_cb  stream_cb; // the function that should handle the response stream
  provider_cb       final_cb;  // the callback the user of the moodle-provider gave us
  SoupMessage      *msg;       // the message we sent: it is usefull as it contains the response headers
  gpointer          user_data; // the data the callback needs
  gchar            *filename;  // the file we are looking for: when caching, the file should be accessible from here.
};

static gchar *
sanitize_filename (const gchar *filename) {
  gchar *clean = g_utf8_make_valid (filename, strlen (filename));
  unsigned long len = strlen (clean);
  g_strdelimit (clean, "/", '-');
  while (*clean == '.' || *clean == '-') {
    memmove (clean, clean+1, len--);
  }
  return clean;
}

G_GNUC_CONST static const gchar *
get_cache_dir (void)
{
  static const gchar *cache_dir;
#ifdef CACHE_ROOT
  cache_dir = CACHE_ROOT;
#else
  cache_dir = g_strconcat (g_get_user_cache_dir (), "/moodle", NULL);
#endif
  g_mkdir (cache_dir, 0700);
  return cache_dir;
}

// get the canonical "file:///" url where a file should be accessible
static gchar *
get_resource_filename (gint res_type, guint id, const gchar *filename)
{
  gchar *filepath;
  switch (res_type) {
  case RESOURCE_FILE:
    if (filename) { // used when we have a file by id and we want to save it to his real name
      g_autofree gchar *sane_filename = sanitize_filename (filename);
      filepath = g_strdup_printf ("file://%s/%s", get_cache_dir (), sane_filename);
    } else {
      filepath = g_strdup_printf ("file://%s/file-%u", get_cache_dir (), id);
    }
    break;
  case RESOURCE_FOLDERFILE:
    filepath = g_strdup_printf ("file://%s/%s", get_cache_dir (), filename);
    break;
  case RESOURCE_FOLDER:
    filepath = g_strdup_printf ("file://%s/folder-%u.html", get_cache_dir (), id);
    break;
  default:
    filepath = NULL;
    g_warning ("unsupported resource type %i\n", res_type);
    break;
  }
  return filepath;
}

static gchar *
get_course_filename (guint id)
{
  gchar *filepath = g_strdup_printf ("file://%s/course-%u.html", get_cache_dir (), id);
  return filepath;
}

static gchar *
get_index_filename (void)
{
  gchar *filepath = g_strdup_printf ("file://%s/index.html", get_cache_dir ());
  return filepath;
}


static void
request_url_cb (GObject      *unused,
                GAsyncResult *res,
                gpointer      info_ptr)
{
  struct cb_data *info = info_ptr;
  gboolean save_to_disk = TRUE;

  if (info->msg->status_code == 1)
    return; // we are aborting

  if (info->msg->status_code != 200) {
    g_warning ("connection error, http %u\n", info->msg->status_code);
    save_to_disk = FALSE;
  }

  GInputStream *is = soup_session_send_finish (self.session,
                                               res,
                                               NULL);

  SoupMessageHeaders *headers;
  g_object_get (G_OBJECT (info->msg), "response-headers", &headers, NULL);
  info->stream_cb (is, save_to_disk, info->filename, headers, info->final_cb, info->user_data);

  g_free (info->filename);
  free (info);
}

// this function is the core of this file. It basically means «get me that»,
// whether I'm online or not and whether it's in cache or not.
// (btw, here's the full prototype, expanding the typedefs:)
// static void request_url (const gchar *url, const gchar *filename, SoupMessagePriority priority, void (*stream_cb) (const gchar *html, gsize html_size, const gchar *filename, void (*cb) (void *user_data, void *retrieved_content), void *user_data), void (*final_cb) (void *user_data, void *retrieved_content), void *user_data);
static void
request_url (const gchar         *url,
             const gchar         *filename,
             SoupMessagePriority  priority,
             handle_stream_cb     stream_cb,
             provider_cb          final_cb,
             gpointer             user_data)
{
  if (self.online_mode == MOODLE_MODE_ONLINE) {

    SoupMessage *msg;
    msg = soup_message_new ("GET", url);
    soup_message_set_priority (msg, priority);
    struct cb_data *cb_info = malloc (sizeof (struct cb_data));
    *cb_info = (struct cb_data)
      { .stream_cb = stream_cb, .final_cb = final_cb, .msg = msg,
        .user_data = user_data, .filename = g_strdup (filename) };
    soup_session_send_async (self.session, msg, NULL, request_url_cb, (gpointer)cb_info);

  } else { // offline_mode

    // this is not quite async, but works «fast enough»
    g_autoptr (GFile) file = g_file_new_for_uri (filename);
    GInputStream *is;
    is = G_INPUT_STREAM (g_file_read (file, NULL, NULL));
    if (G_IS_INPUT_STREAM (is))
      stream_cb (is, FALSE, filename, NULL, final_cb, user_data);
    else
      g_warning ("Could not read file %s from cache\n", filename);

  }
}


static void
moodle_downloaded_file_cb (GInputStream       *is,
                           gboolean            should_save_to_disk,
                           const gchar        *filename_looked_for,
                           SoupMessageHeaders *headers,
                           provider_cb         final_cb,
                           gpointer            user_data)
{
  if (should_save_to_disk) {
    // we first try to read the real name (serie3.pdf)
    // and if it exists, we make file-123456 a symlink to it

    const char *header = soup_message_headers_get_one (headers, "Content-Disposition");
    g_autofree char *real_filename = NULL, *name = NULL;

    if (sscanf (header, "inline;"    " filename=\"%m[^\"]\"", &name) != 1
     && sscanf (header, "attachment;"" filename=\"%m[^\"]\"", &name) != 1) {

      g_warning ("could not read header '%s' to get the filename\n", header);

    } else { // we could get the file name

      real_filename = get_resource_filename (RESOURCE_FILE, 0, name);
      g_autofree gchar *path1 = g_filename_from_uri (real_filename, NULL, NULL);
      g_autofree gchar *path2 = g_filename_from_uri (filename_looked_for, NULL, NULL);

      if (g_strcmp0 (path1, path2) != 0) { // paths are different: make a symlink!
        g_autoptr (GFile) link_file = g_file_new_for_uri (filename_looked_for);
        g_file_make_symbolic_link (link_file, name, NULL, NULL);
        // name is the name without the path: we want to make a relative link, so that's perfect
      }

    }

    write_g_input_stream_to_file (is, real_filename ?: filename_looked_for);
  }

  final_cb (user_data, g_strdup (filename_looked_for));
}

void
moodle_provider_download_file_by_filename_async (guint        folder_id,
                                                 const gchar *moodle_filename,
                                                 provider_cb  cb,
                                                 gpointer     cb_arg)
{
  g_autofree char *url = g_strdup_printf (EPFL_FOLDERFILE_LINK"%s?forcedownload=1", folder_id, moodle_filename);
  g_autofree char *filename = get_resource_filename (RESOURCE_FOLDERFILE, folder_id, moodle_filename);
  request_url (url, filename, SOUP_MESSAGE_PRIORITY_HIGH, moodle_downloaded_file_cb, cb, cb_arg);
}

void
moodle_provider_download_file_async (guint       id,
                                     provider_cb cb,
                                     gpointer    cb_arg)
{
  g_autofree char *url = g_strdup_printf (EPFL_FILE_LINK"%u&redirect=1", id);
  g_autofree char *filename = get_resource_filename (RESOURCE_FILE, id, NULL);
  request_url (url, filename, SOUP_MESSAGE_PRIORITY_HIGH, moodle_downloaded_file_cb, cb, cb_arg);
}


void
moodle_provider_disconnect (void)
{
  soup_session_abort (self.session);
  g_clear_object (&self.session);
  g_clear_object (&self.jar);
  g_clear_object (&self.logger);
}


static enum moodle_error
moodle_provider_connect (GInputStream **response)
{
  SoupMessage *msg;
  char request_key[33];
  /*char *error;
  char *html;
  size_t html_size;*/

  msg = soup_message_new ("GET", EPFL_TEQUILA_LOGIN);
  soup_session_send_message (self.session, msg);
  if (msg->status_code != 200)
    return MOODLE_ERROR_CONNECTION;

  // read request key, needed to auth
  g_autofree gchar *uri = soup_uri_to_string (soup_message_get_uri (msg), FALSE);
  if (!sscanf (strstr (uri, "requestkey=")?:"", "requestkey=%32s", request_key))
    g_error ("impossible to read request key\n");

  g_object_unref (msg);

  msg = soup_form_request_new ("POST",
                               EPFL_TEQUILA_LOGIN_ACTION,
                               "requestkey", request_key,
                               "username", self.username,
                               "password", self.password,
                               NULL);
  *response = soup_session_send (self.session,
                                 msg,
                                 NULL,
                                 NULL);
  if (msg->status_code != 200)
    return MOODLE_ERROR_AUTH;
  //read_g_input_stream (*response, &html, &html_size);
  /*if ((error = moodle_parse_form_error (html, html_size))) {
    g_warning ("Authentification error: %s", error);
    free (html);
    return MOODLE_ERROR_AUTH;
  }
  free (html);*/

  if (g_str_has_prefix (soup_uri_to_string (soup_message_get_uri (msg), FALSE),
                        EPFL_TEQUILA_LOGIN_ACTION)) {
    g_warning ("Did not redirect. This is due to an incorrect password, or to a bug.\n");
    return MOODLE_ERROR_AUTH;
  }

  self.connected = TRUE;

  g_object_unref (msg);
  return MOODLE_ERROR_OK;
}


static void
got_course_content_cb (GInputStream       *is,
                       gboolean            should_save_to_disk,
                       const gchar        *filename,
                       SoupMessageHeaders *headers,
                       provider_cb         final_cb,
                       gpointer            user_data)
{
  g_autofree gchar *html = NULL;
  gsize html_size;
  read_g_input_stream (is, &html, &html_size);
  struct CourseContent content = moodle_parse_course_content (html, html_size, filename);
  if (should_save_to_disk)
    write_file_async (filename, g_steal_pointer (&html), html_size);
  final_cb (user_data, &content);
}


static void
got_folder_content_cb (GInputStream       *is,
                       gboolean            should_save_to_disk,
                       const gchar        *filename,
                       SoupMessageHeaders *headers,
                       provider_cb         final_cb,
                       gpointer            user_data)
{
  g_autofree gchar *html = NULL;
  gsize  html_size;
  read_g_input_stream (is, &html, &html_size);
  struct FolderContent content = moodle_parse_folder_content (html, html_size, filename);
  if (should_save_to_disk)
    write_file_async (filename, g_steal_pointer (&html), html_size);
  final_cb (user_data, &content);
}

void
moodle_provider_get_course_content_async (provider_cb cb, guint id, gpointer cb_arg)
{
  g_autofree char *url = g_strdup_printf (EPFL_COURSE_LINK"%u", id);
  g_autofree char *filename = get_course_filename (id);
  request_url (url, filename, SOUP_MESSAGE_PRIORITY_LOW, got_course_content_cb, cb, cb_arg);
}

enum moodle_error
moodle_provider_get_courses_sync (courses_cb cb,
                                  gpointer   cb_arg)
{
  g_autofree gchar *filename = get_index_filename ();
  g_autofree gchar *html = NULL;
  gsize html_size;
  struct Courses courses;

  if (self.online_mode == MOODLE_MODE_ONLINE) {

    GInputStream *index = NULL;
    // FIXME: unless connected
    enum moodle_error e;
    e = moodle_provider_connect (&index);
    if (e != MOODLE_ERROR_OK) {
      return e;
    }
    read_g_input_stream (index, &html, &html_size);
    courses = moodle_parse_course_list (html, html_size, filename);
    write_file_async (filename, g_steal_pointer (&html), html_size);

  } else { // offline mode

    read_file (filename, &html, &html_size);
    courses = moodle_parse_course_list (html, html_size, filename);

  }

  cb(cb_arg, courses);

  return MOODLE_ERROR_OK;
}


void
moodle_provider_get_folder_content_async (provider_cb cb,
                                          guint       id,
                                          gpointer    cb_arg)
{
  g_autofree char *url = g_strdup_printf (EPFL_FOLDER_LINK"%u", id);
  g_autofree char *filename = get_resource_filename (RESOURCE_FOLDER, id, NULL);

  request_url (url, filename, SOUP_MESSAGE_PRIORITY_HIGH, got_folder_content_cb, cb, cb_arg);
}

void
moodle_provider_init (MoodleOnlineMode  mode,
                      const gchar      *username,
                      const gchar      *password)
{
  self.online_mode = mode;
  self.connected = FALSE;

  moodle_parser_init ();

  self.username = g_strdup (username);
  self.password = g_strdup (password);

  // we init the session even when using cache as it will need to fetch results
  self.session = soup_session_new_with_options (SOUP_SESSION_MAX_CONNS_PER_HOST, 5,
                                                SOUP_SESSION_MAX_CONNS, 5,
                                                SOUP_SESSION_SSL_STRICT, TRUE,
                                                NULL);

  self.jar = soup_cookie_jar_new ();
  soup_session_add_feature (self.session,
                            SOUP_SESSION_FEATURE (self.jar));

  // libsoup logging according to verbosity
  if (LOGGING_REQUESTS) {
    if (LOGGING_REQUESTS == 1)
      self.logger = soup_logger_new (SOUP_LOGGER_LOG_HEADERS, 0);
    else if (LOGGING_REQUESTS == 2)
      self.logger = soup_logger_new (SOUP_LOGGER_LOG_BODY, 100);
    else
      self.logger = soup_logger_new (SOUP_LOGGER_LOG_BODY, 1000000);
    soup_session_add_feature (self.session,
                              SOUP_SESSION_FEATURE (self.logger));
  } else self.logger = NULL;
}

