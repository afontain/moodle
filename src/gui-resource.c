/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "gui-resource.h"
#include "data-struct.h"
#include "moodle-provider.h"
#include "config.h"
#include "utils.h"

#define HANDY_USE_UNSTABLE_API
#include <handy.h>

struct _GuiResource
{
  HdyActionRow parent_instance;
  //HdyActionRow *action;
  //GtkBox *sub_item_list;
  gint type;
  guint id;
  const gchar *filename;
  gboolean downloaded;
};

enum {
  PROP_0, PROP_RESOURCE_TYPE, PROP_RESOURCE_NAME, PROP_RESOURCE_TEXTNAME,
  PROP_RESOURCE_ID, PROP_RESOURCE_FILENAME, N_RESOURCE_PROPERTIES
};

GParamSpec *gui_resource_properties[N_RESOURCE_PROPERTIES] = { NULL, };

G_DEFINE_TYPE (GuiResource, gui_resource, HDY_TYPE_ACTION_ROW)


GuiResource *
gui_resource_new (struct Resource file_info)
{
  g_assert_nonnull (file_info.name);
  return g_object_new (HDY_TYPE_GUI_RESOURCE,
                       "resource_type",     file_info.type,
                       "resource_name",     file_info.name,
                       "resource_textname", file_info.textname,
                       "resource_id",       file_info.id,
                       "resource_filename", file_info.filename,
                       NULL);
}

static void
open_file_cb (gpointer ptr, gpointer filename_ptr)
{
  GuiResource *self = ptr;
  gchar *filename = filename_ptr;
  if (!g_app_info_launch_default_for_uri (filename, NULL, NULL)) {
    g_warning ("Couldn't start a program to open '%s'\n", filename);
    self->downloaded = FALSE; // force download again
  }
}

static void
open_folder_cb (gpointer ptr, gpointer data)
{
  GuiResource *self = ptr;
  struct FolderContent *content = data;
  GtkListBox *parent = GTK_LIST_BOX (gtk_widget_get_parent (GTK_WIDGET (self)));
  guint start_index = gtk_list_box_row_get_index (GTK_LIST_BOX_ROW (self));

  gtk_container_remove (GTK_CONTAINER (parent), GTK_WIDGET (self));
  for (guint i=0; i<content->resource_count; i++) {
    GuiResource *resource = gui_resource_new (content->resources[i]);
    gtk_list_box_insert (parent, GTK_WIDGET (resource), start_index+i);
  }
}

void
activated_cb (GtkListBox* unused, GuiResource *self)
{
  // no need to save if we were sensitive: if it wasn't the case,
  // then this function wouldn't be called.
  gtk_list_box_row_set_activatable (GTK_LIST_BOX_ROW (self), FALSE);

  switch (self->type) {
  case RESOURCE_ASSIGNMENT: {
    // just open it in the web browser
    g_autofree gchar *url = g_strdup_printf (EPFL_ASSIGNMENT_LINK"%u", self->id);
    if (!g_app_info_launch_default_for_uri (url, NULL, NULL))
      g_error ("could not open url %s\n", url);
    break;
  }
  case RESOURCE_FILE:
    moodle_provider_download_file_async (self->id, open_file_cb, self);
    break;
  case RESOURCE_FOLDERFILE:
    if (self->filename && self->id)
      moodle_provider_download_file_by_filename_async (self->id, self->filename, open_file_cb, self);
    else
      g_warning ("That's odd: missing filename and/or id :-/\n");
    break;
  case RESOURCE_FOLDER:
    moodle_provider_get_folder_content_async (open_folder_cb, self->id, self);
    //folder_opened_cb (self, 0, NULL);
    break;
  case RESOURCE_FORUM: {
    // just open it in the web browser
    g_autofree gchar *url = g_strdup_printf (EPFL_FORUM_LINK"%u", self->id);
    if (!g_app_info_launch_default_for_uri (url, NULL, NULL))
      g_error ("could not open url %s\n", url);
    break;
  }
  case RESOURCE_PAGE: {
    // just open it in the web browser
    g_autofree gchar *url = g_strdup_printf (EPFL_PAGE_LINK"%u", self->id);
    if (!g_app_info_launch_default_for_uri (url, NULL, NULL))
      g_error ("could not open url %s\n", url);
    break;
  }
  case RESOURCE_QUIZ: {
    // just open it in the web browser
    g_autofree gchar *url = g_strdup_printf (EPFL_QUIZ_LINK"%u", self->id);
    if (!g_app_info_launch_default_for_uri (url, NULL, NULL))
      g_error ("could not open url %s\n", url);
    break;
  }
  case RESOURCE_URL: {
    // just open it in the web browser, for now
    g_autofree gchar *url = g_strdup_printf (EPFL_URL_LINK"%u", self->id);
    if (!g_app_info_launch_default_for_uri (url, NULL, NULL))
      g_error ("could not open url %s\n", url);
    break;
  }
  default:
    //g_warning ("Ressource type %i isn't supported\n", self->type);
    break;
  }

  gtk_list_box_row_set_activatable (GTK_LIST_BOX_ROW (self), TRUE);
}

static void
gui_resource_set_property (GObject      *object,
                           guint         property_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  GuiResource *self = HDY_GUI_RESOURCE (object);

  switch (property_id) {
  case PROP_RESOURCE_TYPE:
    self->type = g_value_get_int (value);
    switch (self->type) {
    case RESOURCE_ASSIGNMENT:
      hdy_action_row_set_icon_name (HDY_ACTION_ROW (self), "task-due");
      break;
    case RESOURCE_FILE:
    case RESOURCE_FOLDERFILE:
      hdy_action_row_set_icon_name (HDY_ACTION_ROW (self), "x-office-document");
      break;
    case RESOURCE_FOLDER:
      hdy_action_row_set_icon_name (HDY_ACTION_ROW (self), "folder-remote");
      break;
    case RESOURCE_FORUM:
      hdy_action_row_set_icon_name (HDY_ACTION_ROW (self), "x-office-address-book");
      //hdy_action_row_set_icon_name (HDY_ACTION_ROW (self), "user-info");
      break;
    case RESOURCE_PAGE:
      hdy_action_row_set_icon_name (HDY_ACTION_ROW (self), "text-html");
      break;
    case RESOURCE_QUIZ:
      hdy_action_row_set_icon_name (HDY_ACTION_ROW (self), "accessories-text-editor");
      break;
    case RESOURCE_URL:
      //hdy_action_row_set_icon_name (HDY_ACTION_ROW (self), "emblem-web");
      hdy_action_row_set_icon_name (HDY_ACTION_ROW (self), "text-html");
      break;
    default:
      //g_warning ("Ressource type %i isn't supported\n", self->type);
      break;
    }
    break;
  case PROP_RESOURCE_NAME:
    hdy_action_row_set_subtitle (HDY_ACTION_ROW (self), g_value_get_string (value));
    break;
  case PROP_RESOURCE_TEXTNAME:
    hdy_action_row_set_title (HDY_ACTION_ROW (self), g_value_get_string (value));
    break;
  case PROP_RESOURCE_ID:
    self->id = g_value_get_uint (value);
    break;
  case PROP_RESOURCE_FILENAME:
    self->filename = g_strdup (g_value_get_string (value));
    // ask whether we have already downloaded the file
    break;
  default:
    // We don't have any other property...
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void
gui_resource_class_init (GuiResourceClass *class)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  gui_resource_properties[PROP_RESOURCE_TYPE] =
    g_param_spec_int ("resource_type", "type",
                      "the resource type, like RESOURCE_FILE",
                      G_MININT, G_MAXINT, // maybe we could better constrain it
                      -1,
                      G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);

  gui_resource_properties[PROP_RESOURCE_TEXTNAME] =
    g_param_spec_string ("resource_textname", "textname",
                         "The resource textname, like 'Exercices Week 6'",
                         NULL,
                         G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);

  gui_resource_properties[PROP_RESOURCE_NAME] =
    g_param_spec_string ("resource_name", "name",
                         "The resource name, like 'exercises-w6.pdf'",
                         NULL,
                         G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);

  gui_resource_properties[PROP_RESOURCE_ID] =
    g_param_spec_uint ("resource_id", "id",
                       "The resource id, like '102093'",
                       0, G_MAXUINT,
                       0,
                       G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);

  gui_resource_properties[PROP_RESOURCE_FILENAME] =
    g_param_spec_string ("resource_filename", "filename",
                         "The resource filename, like 'serie3.pdf';"
                         " if this property is non-null then it is used instead of `id`",
                         NULL,
                         G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);

  object_class->set_property = gui_resource_set_property;
  g_object_class_install_properties (object_class,
                                     N_RESOURCE_PROPERTIES,
                                     gui_resource_properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/ch/gnugen/Moody/ui/gui-resource.ui");
  //gtk_widget_class_bind_template_child (widget_class, GuiResource, action);
  //gtk_widget_class_bind_template_child (widget_class, GuiResource, sub_item_list);
}


static void
gui_resource_init (GuiResource *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
