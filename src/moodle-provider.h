/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "data-struct.h"

typedef void (*courses_cb)       (gpointer user_data, struct Courses);

typedef void (*provider_cb) (gpointer user_data, gpointer retrieved_content);
// retrieved_content will be a struct Courses *, struct FolderContent *
// according to the called function name.

enum moodle_error {
  MOODLE_ERROR_OK,
  MOODLE_ERROR_CONNECTION,
  MOODLE_ERROR_AUTH,
};

typedef enum moodle_online_mode {
  MOODLE_MODE_ONLINE,
  MOODLE_MODE_OFFLINE,
} MoodleOnlineMode;

void moodle_provider_init (MoodleOnlineMode offline_mode, const gchar *username, const gchar *password);

// close any open connection; deallocate memory.
// use it when closing the program or when restarting the provider
// the provider must be _init'd again if we want to reuse it.
void moodle_provider_disconnect (void);

void moodle_provider_download_file_by_filename_async (guint        folder_id,
                                                      const gchar *filename,
                                                      provider_cb  cb,
                                                      gpointer     cb_arg);

void moodle_provider_download_file_async (guint id, provider_cb cb, gpointer cb_arg);

enum moodle_error moodle_provider_get_courses_sync (courses_cb cb,
                                                    gpointer   cb_arg);

void moodle_provider_get_course_content_async (provider_cb cb,
                                               guint       id,
                                               gpointer    cb_arg);

void moodle_provider_get_folder_content_async (provider_cb cb,
                                               guint       id,
                                               gpointer    cb_arg);

