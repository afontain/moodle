/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "data-struct.h"


struct FolderContent moodle_parse_folder_content (const gchar *html, gsize html_size, const gchar *filename);
struct Courses       moodle_parse_course_list    (const gchar *html, gsize html_size, const gchar *filename);
struct CourseContent moodle_parse_course_content (const gchar *html, gsize html_size, const gchar *filename);

char *moodle_parse_form_error (const char *html, size_t html_size);

void moodle_parser_init (void);


void moodle_parser_course_content_free (struct CourseContent content);
