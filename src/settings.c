/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "settings.h"
#include "password.h"
#include "config.h"

enum {
  PROP_0,
  PROP_SETTING_USERNAME,
  PROP_SETTING_PASSWORD,
  PROP_SETTING_USE_CACHE,
  N_SETTINGS
};

GParamSpec *mood_settings_properties[N_SETTINGS] = { NULL, };

struct _MoodSettings
{
  GObject parent_instance;

  const gchar *username;
  gboolean use_cache;
};

G_DEFINE_TYPE (MoodSettings, mood_settings, G_TYPE_OBJECT)

MoodSettings *
mood_settings_new (void)
{
  return g_object_new (MOOD_TYPE_SETTINGS, NULL);
}


static void
mood_settings_set_property (GObject      *object,
                            guint         property_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  MoodSettings *self = MOOD_MOOD_SETTINGS (object);
  switch (property_id) {
  case PROP_SETTING_USERNAME:
    self->username = g_strdup(g_value_get_string (value));
    break;
  case PROP_SETTING_PASSWORD:
    moodle_password_set (self->username, g_value_get_string (value));
    break;
  case PROP_SETTING_USE_CACHE:
    self->use_cache = g_value_get_boolean (value);
    break;
  default:
    // We don't have any other property...
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void
mood_settings_get_property (GObject    *object,
                            guint       property_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  MoodSettings *self = MOOD_MOOD_SETTINGS (object);
  switch (property_id) {
  case PROP_SETTING_USERNAME:
    if (!self->username)
      g_warning ("That read isn't going well\n");
    g_value_set_string (value, self->username);
    break;
  case PROP_SETTING_PASSWORD: {
    gchar *password;
    if (!moodle_password_get (self->username, &password))
      g_error ("impossible to get saved password\n");
    g_value_set_string (value, password);
    break;
  }
  case PROP_SETTING_USE_CACHE:
    g_value_set_boolean (value, self->use_cache);
    break;
  default:
    // We don't have any other property...
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}


static void
mood_settings_class_init (MoodSettingsClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  //object_class->finalize = &mood_settings_finalize;
  mood_settings_properties[PROP_SETTING_USERNAME] =
    g_param_spec_string ("username", "username",
                         "The username we use to connect to moodle. At EPFL, this is your gaspard account.",
                         NULL,
                         G_PARAM_WRITABLE|G_PARAM_READABLE);

  mood_settings_properties[PROP_SETTING_PASSWORD] =
    g_param_spec_string ("password", "password",
                         "Your account's password",
                         NULL,
                         G_PARAM_WRITABLE|G_PARAM_READABLE);

  mood_settings_properties[PROP_SETTING_USE_CACHE] =
    g_param_spec_boolean ("use_cache", "use_cache",
                          "Whether to save to disk the responses the moodle provider gets",
                          TRUE,
                          G_PARAM_WRITABLE|G_PARAM_READABLE);

  object_class->set_property = &mood_settings_set_property;
  object_class->get_property = &mood_settings_get_property;
  g_object_class_install_properties (object_class,
                                     N_SETTINGS,
                                     mood_settings_properties);
}


static void
mood_settings_init (MoodSettings *self)
{
  //self->settings = g_settings_new (APP_NAME);
  GSettings *settings = g_settings_new (APP_NAME);

  self->username = NULL;
  self->use_cache = -1;

  g_settings_bind (settings, "username",
                   self, "username",
                   G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (settings, "use-cache",
                   self, "use-cache",
                   G_SETTINGS_BIND_DEFAULT);

}