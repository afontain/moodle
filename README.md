
# Moody #

A fast, easy access to your courses

See [CONTRIBUTING.md](/CONTRIBUTING.md) for informations about how it works.

## Quick Install ##

For Arch Linux, there is the [epfl-moody-git](https://aur.archlinux.org/packages/epfl-moody-git/) AUR package.  
Fedora and Debian packages are planned.

### Flatpak Install ###

*for it to work, you may need to [install flatpak](https://flatpak.org/setup/)*

You can also install using flatpak, which should work for every Linux distribution
```sh
flatpak-builder --install-deps-from=flatpak --install --user --force-clean _flatbuild ch.gnugen.Moody.json
```
To build, you may need to install elfutils `sudo apt install elfutils`  
Flatpak will build libhandy and myhtml from their repository, and use libsoup and libsecret
from the Gnome runtime.

You can also install it system-wide by removing the `--user` flag.

## Build it Yourself ##

### Gnome Builder ###

If you are fond of graphical interfaces, get Gnome Builder (`flatpak install org.gnome.Builder`)
and ask it to clone the repositry. Have a look at the code while it gets the dependencies!

### Meson ###

Alternatively, you can install it system-wide outside of the flatpak sandbox;

You first need to install [myhtml](https://github.com/lexborisov/myhtml)
or the modest rendering engine; libhandy, libsecret and libsoup.
You may find these in your repos.

```sh
git clone https://gitlab.gnugen.ch/afontain/moodle.git
cd moodle
meson build-dir
ninja -C build-dir
ninja -C build-dir install
```

## Features ##

### Cache ###

There is a cache system that works pretty well, reusing every file recent enough.
The only thing keeping it from being perfect is that it can't cache-hit the index,
so it is still worked upon in the dev branch.


### Folder Handling ###

You can open folders and access files inside it, which is good.
