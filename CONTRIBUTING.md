
# Contributing
Please do :-) Just ask or make a merge request!


# How it works

here is a loose graph of the dependencies: moody.c contains the `main` function
```
    moody.c
        |
    gui-window.c----------------->|
        |                         |
    gui-course.c----------------->|  
        |                         |
    gui-week.c                    |
        |                         |
    gui-resource.c -------------->|
                                  |
                           moodle-provider.c
                                  |
                           moodle-parser.c
```

0. `main()` opens a new window;
1. The window requests its courses to the moodle provider, creates its gui-courses.
2. Every gui-course request the course content to the moodle provider, and gives the whole info to the children.
3. Each gui-resource can request his file or folder content to the moodle provider.
