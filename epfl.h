/*
 *  Copyright 2019, Antoine Fontaine <antoine.fontaine@epfl.ch>
 *  I release this program under GNU GPLv3+.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#define EPFL_MAIN_PAGE "https://moodle.epfl.ch/my/"
#define EPFL_TEQUILA_LOGIN "https://moodle.epfl.ch/auth/tequila/index.php"
#define EPFL_TEQUILA_LOGIN_ACTION "https://tequila.epfl.ch/cgi-bin/tequila/login"

#define EPFL_ENROL_LINK "https://moodle.epfl.ch/enrol/"

#define EPFL_COURSE_LINK "https://moodle.epfl.ch/course/view.php?id="

#define EPFL_ASSIGNMENT_LINK "https://moodle.epfl.ch/mod/assign/view.php?id="
#define EPFL_FILE_LINK       "https://moodle.epfl.ch/mod/resource/view.php?id="
#define EPFL_FOLDERFILE_LINK "https://moodle.epfl.ch/pluginfile.php/%u/mod_folder/content/0/"
#define EPFL_FOLDER_LINK     "https://moodle.epfl.ch/mod/folder/view.php?id="
#define EPFL_FORUM_LINK      "https://moodle.epfl.ch/mod/forum/view.php?id="
#define EPFL_PAGE_LINK       "https://moodle.epfl.ch/mod/page/view.php?id="
#define EPFL_QUIZ_LINK       "https://moodle.epfl.ch/mod/quiz/view.php?id="
#define EPFL_URL_LINK        "https://moodle.epfl.ch/mod/url/view.php?id="

